# SpringMVC_Shiro_Redis
主要使用SpringMVC、shiro、redis等写个小的权限管理demo
#配置 版本 ：
  - spring:4.1.7.RELEASE;
  - shiro:1.2.5;
  - mybatis:3.3.0;
  - druid:1.0.25;
  - redis:2.8.0;
  
#  项目URL：http://localhost:8080/SpringMVC_Shiro_Redis/login
![输入图片说明](http://git.oschina.net/uploads/images/2016/0923/235515_c673ac2c_498758.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0924/224135_68ec9f1d_498758.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0923/235555_840808e3_498758.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0924/135713_2a77d6b5_498758.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0924/135725_5f7c64fd_498758.png "在这里输入图片标题")
